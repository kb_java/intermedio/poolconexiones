package com.huacachi.poolconexiones.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;

import com.huacachi.poolconexiones.connection_db.Pool;
import com.huacachi.poolconexiones.model.Producto;

public class ProductoDAO {
	
	public static void registrarDB(Producto producto) {
		try (Connection connection = Pool.getConnection()){
			String sql = "INSERT INTO producto (nombre, cantidad, precio, fecha_creacion, fecha_actualizacion) VALUES (?,?,?,?,?)";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, producto.getNombre());
			preparedStatement.setDouble(2, producto.getCantidad());
			preparedStatement.setDouble(3, producto.getPrecio());
			preparedStatement.setDate(4, producto.getFecha_creacion());
			preparedStatement.setDate(5, producto.getFecha_actualizacion());
			
			preparedStatement.executeUpdate();
			
			System.out.println("Producto Registrado Corectamente.!");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
