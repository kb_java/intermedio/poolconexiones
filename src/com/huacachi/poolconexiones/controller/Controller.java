package com.huacachi.poolconexiones.controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.huacachi.poolconexiones.DAO.ProductoDAO;
import com.huacachi.poolconexiones.model.Producto;

/**
 * Servlet implementation class Controller
 */
@WebServlet(description = "Servlet para recibir las peticiones del usuario", urlPatterns = { "/Controller" })
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Controller() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		String opcion = request.getParameter("opcion");
		switch (opcion) {
		case "registrar":
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/View/Registrar.jsp");
			requestDispatcher.forward(request, response);
			break;
		case "listar":
			RequestDispatcher requestDispatcher2 = request.getRequestDispatcher("/View/Listar.jsp");
			requestDispatcher2.forward(request, response);
		case "actualizar":
			RequestDispatcher requestDispatcher3 = request.getRequestDispatcher("/View/Actualizar.jsp");
			requestDispatcher3.forward(request, response);
		default:
			break;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String opcion = request.getParameter("opcion");
		Producto producto = new Producto();
		Date fechaActual = new Date();
		
		switch (opcion) {
		case "registrar":
			producto.setNombre(request.getParameter("txtNombre")); 
			producto.setCantidad(Double.parseDouble(request.getParameter("txtCantidad")));
			producto.setPrecio(Double.parseDouble(request.getParameter("txtPrecio")));
			producto.setFecha_creacion(new java.sql.Date(fechaActual.getTime()));
			
			ProductoDAO.registrarDB(producto);
			System.out.println("Se Registr� Correctamente..!!");
						
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("Index.jsp");
			requestDispatcher.forward(request, response);
			break;

		default:
			break;
		}
		
	}

}
