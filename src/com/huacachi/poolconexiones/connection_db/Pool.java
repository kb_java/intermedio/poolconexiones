package com.huacachi.poolconexiones.connection_db;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;

public class Pool {
	private static BasicDataSource basicDataSource = null;
	
	public static DataSource dataSource() {
		if(basicDataSource == null) {
			basicDataSource = new BasicDataSource();
			basicDataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
			basicDataSource.setUsername("ehuacachi");
			basicDataSource.setPassword("9xPiwZ3r");
			basicDataSource.setUrl("jdbc:oracle:thin:@localhost:1521:xe");
			//Definimos el tama�o de conexiones
			basicDataSource.setInitialSize(50); //50 conexiones iniciales
			basicDataSource.setMaxIdle(10);
			basicDataSource.setMaxTotal(20);
			basicDataSource.setMaxWaitMillis(5000);
			
		}
		return basicDataSource;
	}
	
	public static Connection getConnection() throws SQLException {
		return dataSource().getConnection();
	}

}
